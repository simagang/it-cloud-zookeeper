package com.itcloud.zk.base;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.*;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 事件监听API使用
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CacheApiTests {

    private CuratorFramework client;

    /**
     * 使用Fluent风格API创建
     */
    @Before
    public void client() {
        // 重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        client = CuratorFrameworkFactory.builder()
                .connectString("localhost:2181")
                .sessionTimeoutMs(5000)  // 会话超时时间，默认60000ms
                .connectionTimeoutMs(10000) // 连接超时时间，默认15000ms
                .retryPolicy(retryPolicy) // 重试策略
                .namespace("base") // 名称空间，如果这里设置了名称空间，那么所有路径都将预先指定名称空间
                .build();
        client.start();
    }

    /**
     * NodeCache
     */
    @Test
    public void nodeCache() throws Exception {
        String path = "/node-cache";
        // 创建节点并赋值
        client.create()
                .creatingParentsIfNeeded()
                .withMode(CreateMode.EPHEMERAL)
                .forPath(path, "sima".getBytes());
        // 参数3：是否对数据进行压缩
        final NodeCache cache = new NodeCache(client, path, false);
        // 参数1：默认false，如果设置true代表第一次启动的时候从ZK上读取对应节点的数据内容
        cache.start(true);
        cache.getListenable().addListener(new NodeCacheListener() {
            @Override
            public void nodeChanged() throws Exception {
                // getCurrentData()得到节点当前的状态
                ChildData data = cache.getCurrentData();
                if (null != data) {
                    System.out.println("节点数据更新：" + new String(data.getData()));
                } else {
                    System.out.println("节点被删除");
                }
            }
        });
        // 更新数据
        client.setData().forPath(path, "gang".getBytes());
        Thread.sleep(1000);
        // 删除节点
        client.delete().deletingChildrenIfNeeded().forPath(path);
        Thread.sleep(Integer.MAX_VALUE);

       /* cache.close();
        client.close();*/
    }

    /**
     * PathChildrenCache
     */
    @Test
    public void pathChildrenCache() throws Exception {
        String path = "/path-cache3";
        // 参数3：缓存节点内容，如果设置true，那么客户端在接收到节点列表变更的同时，也能够获取节点的数据内容
        // 参数4（可选）：threadFactory和executorService可以使用单独的线程池来处理事件通知
        PathChildrenCache cache = new PathChildrenCache(client, path, true);
        // 启动模式：
        // NORMAL: 初始化缓存数据为空
        // BUILD_INITIAL_CACHE: 在start方法返回前，初始化获取每个子节点数据并缓存
        // POST_INITIALIZED_EVENT: 在后台异步初始化数据完成后，会发送一个INITIALIZED初始化完成事件
        cache.start(PathChildrenCache.StartMode.POST_INITIALIZED_EVENT);
        cache.getListenable().addListener(new PathChildrenCacheListener() {
            public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {
                switch (event.getType()) {
                    // 初始化
                    // case INITIALIZED:
                    //    System.out.println("INITIALIZED," + event.getData().getPath());
                    //    break;
                    // 添加子节点
                    case CHILD_ADDED:
                        System.out.println("CHILD_ADDED," + event.getData().getPath());
                        break;
                    // 子节点数据变更
                    case CHILD_UPDATED:
                        System.out.println("CHILD_UPDATED," + event.getData().getPath());
                        break;
                    // 子节点删除
                    case CHILD_REMOVED:
                        System.out.println("CHILD_REMOVED," + event.getData().getPath());
                        break;
                    default:
                        break;
                }
            }
        });
        // 创建节点
        client.create().withMode(CreateMode.PERSISTENT).forPath(path);
        Thread.sleep(1000);
        // 创建子节点
        client.create().withMode(CreateMode.PERSISTENT).forPath(path + "/z2");
        Thread.sleep(1000);
        // 删除子节点
        client.delete().forPath(path + "/z2");
        Thread.sleep(1000);
        // 删除节点
        client.delete().forPath(path);
        Thread.sleep(Integer.MAX_VALUE);
    }

    /**
     * PathChildrenCache--带线程池
     */
    @Test
    public void pathChildrenCacheWithThread() throws Exception {
        String path = "/path-cache1";
        ExecutorService ex = Executors.newFixedThreadPool(2);
        // 参数3：缓存节点内容，如果设置true，那么客户端在接收到节点列表变更的同时，也能够获取节点的数据内容
        // 参数4（可选）：threadFactory和executorService可以使用单独的线程池来处理事件通知
        PathChildrenCache cache = new PathChildrenCache(client, path, true, false, ex);
        // 启动模式：
        // NORMAL: 初始化缓存数据为空
        // BUILD_INITIAL_CACHE: 在start方法返回前，初始化获取每个子节点数据并缓存
        // POST_INITIALIZED_EVENT: 在后台异步初始化数据完成后，会发送一个INITIALIZED初始化完成事件
        cache.start(PathChildrenCache.StartMode.POST_INITIALIZED_EVENT);
        cache.getListenable().addListener(new PathChildrenCacheListener() {
            public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {
                switch (event.getType()) {
                    // 初始化
                    case INITIALIZED:
                        System.out.println("INITIALIZED," + event.getData().getPath());
                        break;
                    // 添加子节点
                    case CHILD_ADDED:
                        System.out.println("CHILD_ADDED," + event.getData().getPath());
                        break;
                    // 子节点数据变更
                    case CHILD_UPDATED:
                        System.out.println("CHILD_UPDATED," + event.getData().getPath());
                        break;
                    // 子节点删除
                    case CHILD_REMOVED:
                        System.out.println("CHILD_REMOVED," + event.getData().getPath());
                        break;
                    default:
                        break;
                }
            }
        });
        // 创建节点
        client.create().withMode(CreateMode.PERSISTENT).forPath(path);
        Thread.sleep(1000);
        // 创建子节点
        client.create().withMode(CreateMode.PERSISTENT).forPath(path + "/z2");
        Thread.sleep(1000);
        // 删除子节点
        client.delete().forPath(path + "/z2");
        Thread.sleep(1000);
        // 删除节点
        client.delete().forPath(path);
        Thread.sleep(Integer.MAX_VALUE);
    }

    /**
     * TreeCache
     */
    @Test
    public void TreeCache() throws Exception {
        String path = "/tree-cache";
        // 创建节点
        client.create().creatingParentsIfNeeded().forPath(path);
        TreeCache cache = new TreeCache(client, path);
        TreeCacheListener listener = (client1, event) ->
                System.out.println("事件类型：" + event.getType() +
                        ",路径：" + (null != event.getData() ? event.getData().getPath() : null));
        cache.getListenable().addListener(listener);
        cache.start();
        // 更新数据
        client.setData().forPath(path, "1".getBytes());
        Thread.sleep(1000);
        // 更新数据
        client.setData().forPath(path, "2".getBytes());
        Thread.sleep(1000);
        // 删除节点
        client.delete().deletingChildrenIfNeeded().forPath(path);
        Thread.sleep(1000 * 2);
        cache.close();
        client.close();
    }

}
