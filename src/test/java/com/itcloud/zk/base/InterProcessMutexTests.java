package com.itcloud.zk.base;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 分布式锁InterProcessMutex
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class InterProcessMutexTests {

    private CuratorFramework client;

    /**
     * 使用Fluent风格API创建
     */
    @Before
    public void client() {
        // 重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        client = CuratorFrameworkFactory.builder()
                .connectString("localhost:2181")
                .sessionTimeoutMs(5000)  // 会话超时时间，默认60000ms
                .connectionTimeoutMs(10000) // 连接超时时间，默认15000ms
                .retryPolicy(retryPolicy) // 重试策略
                // .namespace("base") // 名称空间，如果这里设置了名称空间，那么所有路径都将预先指定名称空间
                .build();
        client.start();
    }

    /**
     * InterProcessMutex事例：生成订单号
     */
    @Test
    public void InterProcessMutex() throws Exception {
        String lockPath = "/lock_path";
        InterProcessMutex lock = new InterProcessMutex(client, lockPath);
        // 模拟10个线程抢锁
        for (int i = 0; i < 10; i++) {
            new Thread(new TestThread(i, lock)).start();
        }
        Thread.sleep(Integer.MAX_VALUE);
    }

    static class TestThread implements Runnable {
        private Integer n;
        private InterProcessMutex lock;

        public TestThread(Integer n, InterProcessMutex lock) {
            this.n = n;
            this.lock = lock;
        }

        @Override
        public void run() {
            try {
                lock.acquire();
                System.out.println("第" + n + "线程获取到了锁");
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss|SSS");
                String orderNo = sdf.format(new Date());
                System.out.println("生成的订单号是 : " + orderNo);
                // 1秒后释放锁
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    lock.release();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
