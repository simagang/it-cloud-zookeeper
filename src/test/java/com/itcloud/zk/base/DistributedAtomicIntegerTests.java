package com.itcloud.zk.base;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.atomic.AtomicValue;
import org.apache.curator.framework.recipes.atomic.DistributedAtomicInteger;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.retry.RetryNTimes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 分布式计数器 DistributedAtomicInteger
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class DistributedAtomicIntegerTests {

    private CuratorFramework client;

    /**
     * 使用Fluent风格API创建
     */
    @Before
    public void client() {
        // 重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        client = CuratorFrameworkFactory.builder()
                .connectString("localhost:2181")
                .sessionTimeoutMs(5000)  // 会话超时时间，默认60000ms
                .connectionTimeoutMs(10000) // 连接超时时间，默认15000ms
                .retryPolicy(retryPolicy) // 重试策略
                .namespace("base") // 名称空间，如果这里设置了名称空间，那么所有路径都将预先指定名称空间
                .build();
        client.start();
    }

    @Test
    public void test() throws Exception {
        String path = "/atomic_path3";
        // 参数3: 更新重试策略
        DistributedAtomicInteger atomicInteger =
                new DistributedAtomicInteger(client, path, new RetryNTimes(3, 1000));
        // +8
        AtomicValue<Integer> atomicValue = atomicInteger.add(8);
        System.out.println("结果: " + atomicValue.succeeded() + "，值：" + atomicValue.postValue());
        System.out.println("初始值：" + atomicValue.preValue());
    }

}
