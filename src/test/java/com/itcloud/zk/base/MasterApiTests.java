package com.itcloud.zk.base;

import com.google.common.collect.Lists;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.leader.LeaderLatch;
import org.apache.curator.framework.recipes.leader.LeaderSelector;
import org.apache.curator.framework.recipes.leader.LeaderSelectorListenerAdapter;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;

/**
 * Master选择
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class MasterApiTests {

    private CuratorFramework client;

    /**
     * 使用Fluent风格API创建
     */
    @Before
    public void client() {
        // 重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        client = CuratorFrameworkFactory.builder()
                .connectString("localhost:2181")
                .sessionTimeoutMs(5000)  // 会话超时时间，默认60000ms
                .connectionTimeoutMs(10000) // 连接超时时间，默认15000ms
                .retryPolicy(retryPolicy) // 重试策略
                .namespace("base") // 名称空间，如果这里设置了名称空间，那么所有路径都将预先指定名称空间
                .build();
        client.start();
    }

    /**
     * LeaderSelector
     */
    @Test
    public void LeaderSelector() throws Exception {
        String path = "/master-path";
        int CLINET_COUNT = 10;
        List<CuratorFramework> clientsList = Lists.newArrayListWithCapacity(CLINET_COUNT);
        List<LeaderSelector> latchList = Lists.newArrayListWithCapacity(CLINET_COUNT);

        // 创建10个zk客户端模拟leader选举
        for (int i = 0; i < CLINET_COUNT; i++) {
            CuratorFramework client = getZkClient();
            clientsList.add(client);
            // 可以多个参数3：ExecutorService： Master选举单独使用的线程池
            // public LeaderSelector(CuratorFramework client, String leaderPath, ExecutorService executorService, LeaderSelectorListener listener)
            LeaderSelector selector = new LeaderSelector(client, path,
                    new LeaderSelectorListenerAdapter() {
                        public void takeLeadership(CuratorFramework client) throws Exception {
                            System.out.println(Thread.currentThread().getName() + "成为了Master");
                            Thread.sleep(3000);
                            System.out.println("释放Master权利");
                        }
                    });
            latchList.add(selector);
        }


        latchList.forEach(leaderSelector -> {
            // 重新获得竞争权利
            leaderSelector.autoRequeue();
            // 启动选举
            leaderSelector.start();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Thread.sleep(Integer.MAX_VALUE);
    }

    /**
     * LeaderLatch
     */
    @Test
    public void LeaderLatch() throws Exception {
        // 客户端数
        int CLINET_COUNT = 10;
        String path = "/latch-path";

        List<CuratorFramework> clientsList = Lists.newArrayListWithCapacity(CLINET_COUNT);
        List<LeaderLatch> latchList = Lists.newArrayListWithCapacity(CLINET_COUNT);
        // 创建10个zk客户端模拟leader选举
        for (int i = 0; i < CLINET_COUNT; i++) {
            CuratorFramework client = getZkClient();
            clientsList.add(client);
            // 参数3：客户端ID
            // 参数4：关闭策略，SILENT-关闭时不触发监听器回调，NOTIFY_LEADER-关闭时触发监听器回调方法，默认不触发
            LeaderLatch leaderLatch = new LeaderLatch(client, path, "CLIENT_" + i);
            latchList.add(leaderLatch);
            // 调用start方法开始抢主
            leaderLatch.start();
        }
        // 判断当前leader是哪个客户端
        checkLeader(latchList);

    }

    private void checkLeader(List<LeaderLatch> leaderLatchList) throws Exception {
        // 睡5秒
        Thread.sleep(5000);
        for (LeaderLatch leaderLatch : leaderLatchList) {
            // 判断当前客户端是否是leader
            if (leaderLatch.hasLeadership()) {
                System.out.println("当前leader:" + leaderLatch.getId());
                // 释放leader权限
                leaderLatch.close();
                // 检查当前leader
                checkLeader(leaderLatchList);
            }
        }
    }

    private CuratorFramework getZkClient() {
        ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(1000, 3, 5000);
        CuratorFramework zkClient = CuratorFrameworkFactory.builder()
                .connectString("localhost:2181")
                .sessionTimeoutMs(5000)  // 会话超时时间，默认60000ms
                .connectionTimeoutMs(10000) // 连接超时时间，默认15000ms
                .retryPolicy(retryPolicy) // 重试策略
                .build();
        zkClient.start();
        return zkClient;
    }


}
