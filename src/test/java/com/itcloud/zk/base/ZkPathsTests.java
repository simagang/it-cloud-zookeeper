package com.itcloud.zk.base;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.EnsurePath;
import org.apache.curator.utils.ZKPaths;
import org.apache.zookeeper.ZooKeeper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * ZKPaths事例
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ZkPathsTests {

    static String path = "/zkpath_path";

    private CuratorFramework client;

    /**
     * 使用Fluent风格API创建
     */
    @Before
    public void client() {
        // 重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        client = CuratorFrameworkFactory.builder()
                .connectString("localhost:2181")
                .sessionTimeoutMs(5000)  // 会话超时时间，默认60000ms
                .connectionTimeoutMs(10000) // 连接超时时间，默认15000ms
                .retryPolicy(retryPolicy) // 重试策略
                // .namespace("base") // 名称空间，如果这里设置了名称空间，那么所有路径都将预先指定名称空间
                .build();
        client.start();
    }


    @Test
    public void test() throws Exception {
        ZooKeeper zookeeper = client.getZookeeperClient().getZooKeeper();

        // 将名称空间应用于给定的路径
        System.out.println(ZKPaths.fixForNamespace("/zkpath_path", "/sub1"));
        // 给定父路径和子节点，创建一个组合的完整路径
        System.out.println(ZKPaths.makePath(path, "/sub1"));
        // 给定完整路径，返回节点名。即。"/cluster/sub2"会返回"sub2"
        System.out.println(ZKPaths.getNodeFromPath("/cluster/sub2"));
        // 给定完整路径，返回节点名及其路径。即。"/cluster/sub2"将返回{"/cluster"，"sub2"}
        ZKPaths.PathAndNode pn = ZKPaths.getPathAndNode("/cluster/sub2");
        System.out.println(pn.getPath());
        System.out.println(pn.getNode());

        String dir1 = path + "/child3";
        String dir2 = path + "/child4";
        // 创建节点，会创建所有包含节点
        ZKPaths.mkdirs(zookeeper, dir1);
        ZKPaths.mkdirs(zookeeper, dir2);
        // 得到子节点集合
        System.out.println(ZKPaths.getSortedChildren(zookeeper, path));
        // 递归地删除节点的子节点
        ZKPaths.deleteChildren(client.getZookeeperClient().getZooKeeper(), path, true);
        // 关闭工具类
        // CloseableUtils.closeQuietly(queue);
        // CloseableUtils.closeQuietly(client);
    }

}
