package com.itcloud.zk.base;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.barriers.DistributedBarrier;
import org.apache.curator.framework.recipes.barriers.DistributedDoubleBarrier;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 分布式 Barrier
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class BarrierTests {

    static String barrier_path = "/barrier_path";
    static DistributedBarrier barrier;

    /**
     * 第一种
     */
    @Test
    public void BarrierTest() throws Exception {
        for (int i = 0; i < 5; i++) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        CuratorFramework client = getZkClient();
                        barrier = new DistributedBarrier(client, barrier_path);
                        System.out.println(Thread.currentThread().getName() + "barrier设置");
                        // 完成barrier设置
                        barrier.setBarrier();
                        // 等待barrier释放
                        barrier.waitOnBarrier();
                        System.err.println("启动");
                    } catch (Exception e) {
                    }
                }
            }).start();
        }
        Thread.sleep(2000);
        // 释放barrier
        barrier.removeBarrier();
    }

    /**
     * 第二种
     */
    @Test
    public void BarrierTest1() throws Exception {
        for (int i = 0; i < 5; i++) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        CuratorFramework client = getZkClient();
                        DistributedDoubleBarrier barrier = new DistributedDoubleBarrier(client, barrier_path, 5);
                        Thread.sleep(Math.round(Math.random() * 3000));
                        System.out.println(Thread.currentThread().getName() + "进入barrier");
                        // 进入准备状态，等待进去barrier成员达到5个
                        barrier.enter();
                        System.out.println("启动");
                        Thread.sleep(Math.round(Math.random() * 3000));
                        // 进入准备退出状态，等待退出barrier成员达到5个
                        barrier.leave();
                        System.out.println("退出");
                    } catch (Exception e) {
                    }
                }
            }).start();
        }
        Thread.sleep(10000);
    }

    private CuratorFramework getZkClient() {
        ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(1000, 3, 5000);
        CuratorFramework zkClient = CuratorFrameworkFactory.builder()
                .connectString("localhost:2181")
                .sessionTimeoutMs(5000)  // 会话超时时间，默认60000ms
                .connectionTimeoutMs(10000) // 连接超时时间，默认15000ms
                .retryPolicy(retryPolicy) // 重试策略
                .build();
        zkClient.start();
        return zkClient;
    }

}
