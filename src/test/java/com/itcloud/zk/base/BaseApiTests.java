package com.itcloud.zk.base;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.BackgroundCallback;
import org.apache.curator.framework.api.CuratorEvent;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 基本API使用
 */

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class BaseApiTests {

    private CuratorFramework client;

    /**
     * 使用Fluent风格API创建
     */
    @Before
    public void client() {
        // 重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        client = CuratorFrameworkFactory.builder()
                .connectString("localhost:2181")
                .sessionTimeoutMs(5000)  // 会话超时时间，默认60000ms
                .connectionTimeoutMs(10000) // 连接超时时间，默认15000ms
                .retryPolicy(retryPolicy) // 重试策略
                .namespace("base") // 名称空间，如果这里设置了名称空间，那么所有路径都将预先指定名称空间
                .build();
        client.start();
    }

    /**
     * 使用静态工厂创建
     */
    @Test
    public void client1() throws Exception {
        // 重试策略
        RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.newClient(
                "localhost:2181",
                5000, // 会话超时时间，默认60000ms
                10000, // 连接超时时间，默认15000ms
                retryPolicy);
        client.start();
        Thread.sleep(Integer.MAX_VALUE);
    }

    /**
     * 创建节点并初始化值，默认是持久节点
     */
    @Test
    public void create1() throws Exception {
        String path = "/create";
        client.create()
                .forPath(path, "init".getBytes()); // 指定路径和值
    }

    /**
     * 创建临时节点
     */
    @Test
    public void create2() throws Exception {
        String path = "/create1";
        client.create()
                .withMode(CreateMode.EPHEMERAL) // 创建临时节点
                .forPath(path, "init".getBytes());
        Thread.sleep(10000); // 10s后消失
    }

    /**
     * 递归创建所需要的父节点，ZK中所有非叶子节点必须是持久节点
     */
    @Test
    public void create3() throws Exception {
        String path = "/zk-test/z1";
        client.create()
                .creatingParentsIfNeeded() // 递归创建父节点
                .withMode(CreateMode.EPHEMERAL) // 创建临时节点
                .forPath(path, "init".getBytes());
        Thread.sleep(Integer.MAX_VALUE);
    }

    /**
     * 删除节点
     */
    @Test
    public void delete() throws Exception {
        client.delete()
                .guaranteed()  // 强制保证删除，只要客户端会话存在，就会一直删除，直到成功。
                // 当客户端遇到网络异常，会记录这次删除操作
                .deletingChildrenIfNeeded() // 递归删除子节点
                // .withVersion(1) // 指定删除的版本号（可选）
                .forPath("/create2");
    }

    /**
     * 读取数据
     */
    @Test
    public void get() throws Exception {
        byte[] bytes = client.getData().forPath("/create");
        System.out.println(new String(bytes));
    }

    /**
     * 读取数据，同时获取该节点stat
     */
    @Test
    public void get1() throws Exception {
        Stat stat = new Stat();
        client.getData()
                .storingStatIn(stat)
                .forPath("/create");
    }

    /**
     * 更新数据
     */
    @Test
    public void set() throws Exception {
        client.setData()
                //.withVersion(2) // 指定版本修改（可选）
                .forPath("/create", "init1".getBytes());
    }

    /**
     * 检查节点是否存在
     */
    @Test
    public void exists() throws Exception {
        client.checkExists().forPath("/create");
    }

    /**
     * 获取某个节点的所有子节点路径
     */
    @Test
    public void getChildren() throws Exception {
        List<String> list = client.getChildren().forPath("/");
        list.forEach(item ->{
            System.out.println(item);
        });
    }

    /**
     * 事务
     */
    @Test
    public void transaction() throws Exception {
        client.inTransaction().check().forPath("/create")
                .and()
                .create().withMode(CreateMode.EPHEMERAL).forPath("/nodeB", "init".getBytes())
                .and()
                .create().withMode(CreateMode.EPHEMERAL).forPath("/nodeC", "init".getBytes())
                .and()
                .commit();
    }

    /**
     * 异步创建节点
     */
    @Test
    public void createSync() throws Exception {
        String path = "/create2";
        CountDownLatch semaphore = new CountDownLatch(2);
        ExecutorService tp = Executors.newFixedThreadPool(2);

        System.out.println("Main thread: " + Thread.currentThread().getName());
        // 自定义的Executor
        client.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).inBackground(new BackgroundCallback() {
            @Override
            public void processResult(CuratorFramework client, CuratorEvent event) throws Exception {
                System.out.println("event[code: " + event.getResultCode() + ", type: " + event.getType() + "]");
                System.out.println("Thread name: " + Thread.currentThread().getName());
                semaphore.countDown();
            }
        }, tp).forPath(path, "init".getBytes());
        // 此处没有传入自定义的Executor
        client.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).inBackground(new BackgroundCallback() {
            @Override
            public void processResult(CuratorFramework client, CuratorEvent event) throws Exception {
                System.out.println("event[code: " + event.getResultCode() + ", type: " + event.getType() + "]");
                System.out.println("Thread name: " + Thread.currentThread().getName());
                semaphore.countDown();
            }
        }).forPath(path, "init".getBytes());

        semaphore.await();
        tp.shutdown();
    }


}
