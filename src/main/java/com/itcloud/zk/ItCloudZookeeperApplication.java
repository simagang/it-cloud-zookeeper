package com.itcloud.zk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItCloudZookeeperApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItCloudZookeeperApplication.class, args);
	}

}
